package sort

func quicksort(nums []int) {
	aaa(nums, 0, len(nums)-1)
}

func aaa(nums []int, x, y int) {
	if x >= y {
		return
	}

	p := x
	for i := x; i < y; i++ {
		if nums[i] < nums[y] {
			nums[i], nums[p] = nums[p], nums[i]
			p++
		}
	}

	nums[y], nums[p] = nums[p], nums[y]
	aaa(nums, x, p-1)
	aaa(nums, p+1, y)
}
