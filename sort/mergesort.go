package sort

// other go style implement: https://play.golang.org/p/Ma2GXvj3XP
func mergesort(nums []int) []int {
	l := len(nums)
	if l <= 1 {
		return nums
	}

	mid := l / 2
	nums1 := mergesort(nums[:mid])
	nums2 := mergesort(nums[mid:])
	return merge(nums1, nums2)

}

func merge(nums1 []int, nums2 []int) []int {
	l1 := len(nums1)
	l2 := len(nums2)
	s1, s2 := 0, 0
	nums := make([]int, l1+l2)
	for i := 0; i < l1+l2; i++ {
		if s1 < l1 && s2 < l2 {
			if nums1[s1] < nums2[s2] {
				nums[i] = nums1[s1]
				s1++
			} else {
				nums[i] = nums2[s2]
				s2++
			}
		} else if s1 >= l1 {
			nums[i] = nums2[s2]
			s2++
		} else if s2 >= l2 {
			nums[i] = nums1[s1]
			s1++
		}
	}

	return nums
}
