package sort

import "testing"

func TestHeapsort(t *testing.T) {
	nums := []int{3, 5, 3, 0, 8, 6, 1, 5, 8, 6, 2, 4, 9, 4, 7, 0, 1, 8, 9, 7, 3, 1, 2, 5, 9, 7, 4, 0, 2, 6}
	// asc sort
	heapsort(nums)
	for i := 0; i < len(nums)-1; i++ {
		if nums[i] > nums[i+1] {
			t.Errorf("[%d] %d should smaller than %d", i, nums[i], nums[i+1])
		}
	}
}

func TestQuicksort(t *testing.T) {
	nums := []int{1, 4, 0, 5, 3, 9, 6, 7, 2, 8}
	quicksort(nums)
	for i := 0; i < len(nums)-1; i++ {
		if nums[i] > nums[i+1] {
			t.Errorf("[%d] %d should smaller than %d", i, nums[i], nums[i+1])
		}
	}
}

func TestMergesort(t *testing.T) {
	nums := []int{1, 4, 0, 5, 3, 9, 6, 7, 2, 8}
	nums = mergesort(nums)
	t.Logf("%+v", nums)
	for i := 0; i < len(nums)-1; i++ {
		if nums[i] > nums[i+1] {
			t.Errorf("[%d] %d should smaller than %d", i, nums[i], nums[i+1])
		}
	}
}
