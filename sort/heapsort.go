package sort

func heapsort(nums []int) {
	l := len(nums)
	for i := l/2 - 1; i >= 0; i-- {
		maxHeapify(nums, i, l-1)
	}

	for i := l - 1; i > 0; i-- {
		nums[i], nums[0] = nums[0], nums[i]
		maxHeapify(nums, 0, i-1)
	}
}

func maxHeapify(nums []int, start, end int) {
	dad := start
	son := dad*2 + 1
	for son <= end {
		if son+1 <= end && nums[son] < nums[son+1] {
			son++
		}

		if nums[dad] > nums[son] {
			return
		}

		nums[dad], nums[son] = nums[son], nums[dad]
		dad = son
		son = dad*2 + 1
	}
}
