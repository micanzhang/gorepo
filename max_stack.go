package gorepo

import (
	"fmt"

	"github.com/micanzhang/acode/container"
)

type MaxStack struct {
	stack container.Stack
	max   int
}

func (s *MaxStack) Push(x int) {
	if s.stack.IsEmpty() {
		s.stack.Push(0)
		s.max = x
		return
	}

	s.stack.Push(s.max - x)
	if x > s.max {
		s.max = x
	}
}

func (s *MaxStack) Pop() {
	v := s.stack.Pop()
	if v == nil {
		return
	}

	n := v.(int)
	if n < 0 {
		s.max = s.max + n
	}
}

func (s *MaxStack) Top() int {
	v := s.stack.Peek()
	if v == nil {
		return 0
	}

	fmt.Println(v, s.max)
	n := v.(int)
	if n < 0 {
		return s.max
	}

	return s.max - n
}

func (s *MaxStack) GetMax() int {
	return s.max
}
