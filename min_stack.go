package gorepo

import (
	"github.com/micanzhang/gorepo/container"
)

type MinStack struct {
	stack container.Stack
	min   int
}

func (s *MinStack) Push(x int) {
	if s.stack.IsEmpty() {
		s.stack.Push(0)
		s.min = x
		return
	}

	s.stack.Push(x - s.min)
	if x < s.min {
		s.min = x
	}
}

func (s *MinStack) Pop() {
	v := s.stack.Pop()
	if v == nil {
		return
	}

	n := v.(int)
	if n < 0 {
		s.min = s.min - n
	}
}

func (s *MinStack) Top() int {
	v := s.stack.Peek()
	if v == nil {
		return 0
	}

	n := v.(int)
	if n < 0 {
		return s.min
	}

	return n + s.min
}

func (s *MinStack) GetMin() int {
	return s.min
}
