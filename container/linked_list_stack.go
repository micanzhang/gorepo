package container

import (
	"sync"
)

type Element struct {
	Val  interface{}
	Next *Element
}

// LinkedListStack implement stack data strutrue use a singly linked list.
type LinkedListStack struct {
	sync.Mutex
	Top  *Element
	size int
}

func NewLinkedListStack() Stack {
	return &LinkedListStack{}
}

func (s *LinkedListStack) Size() int {
	s.Lock()
	defer s.Unlock()

	return s.size
}

func (s *LinkedListStack) IsEmpty() bool {
	return s.Size() == 0
}

func (s *LinkedListStack) Push(e interface{}) {
	s.Lock()
	defer s.Unlock()

	node := &Element{
		Val:  e,
		Next: s.Top,
	}

	s.Top = node
	s.size++
}

func (s *LinkedListStack) Pop() interface{} {
	if s.IsEmpty() {
		return nil
	}

	s.Lock()
	defer s.Unlock()

	e := s.Top
	s.Top = e.Next
	s.size--

	return e.Val
}

func (s *LinkedListStack) Peek() interface{} {
	if s.IsEmpty() {
		return nil
	}

	s.Lock()
	defer s.Unlock()

	return s.Top.Val
}
