package container

// List In First Out
type Stack interface {
	IsEmpty() bool
	Push(interface{})
	Pop() interface{}
	Peek() interface{}
	Size() int
}
