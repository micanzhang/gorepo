package container

import "sync"

type ArrayStack struct {
	sync.Mutex
	elements []interface{}
	size     int
}

func NewArrayStack() Stack {
	return &ArrayStack{
		elements: make([]interface{}, 0),
	}
}

func (s *ArrayStack) Size() int {
	s.Lock()
	defer s.Unlock()

	return s.Size()
}

func (s *ArrayStack) IsEmpty() bool {
	return s.size == 0
}

func (s *ArrayStack) Push(e interface{}) {
	s.Lock()
	defer s.Unlock()

	s.elements = append(s.elements, e)
	s.size++
}

func (s *ArrayStack) Pop() interface{} {
	if s.IsEmpty() {
		return nil
	}

	s.Lock()
	defer s.Unlock()

	s.size--
	e := s.elements[s.size]
	s.elements = s.elements[:s.size]

	return e
}

func (s *ArrayStack) Peek() interface{} {
	if s.IsEmpty() {
		return nil
	}

	s.Lock()
	defer s.Unlock()

	return s.elements[s.size-1]
}
