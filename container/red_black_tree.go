package container

/*
Wiki: https://en.wikipedia.org/wiki/Red%E2%80%93black_tree

properties:
1. every node are red or black
2. root node is black
3. new inseration node is red
4. null node is black
5. the number of black nodes from root to leaf must be equal for every possible path
6. no two red nodes are consecutive
*/

type RedBlackNode struct {
	Key     interface{}
	Value   interface{}
	Left    *RedBlackNode
	Right   *RedBlackNode
	Parent  *RedBlackNode
	IsLeft  bool
	IsBlack bool
}

func NewRedBlackNode(key, value interface{}) *RedBlackNode {
	return nil
}

type RedBlackTree struct {
	root *RedBlackNode
	size int
}

func NewRedBlackTree() *RedBlackTree {
	return &RedBlackTree{}
}

func (t *RedBlackTree) Insert(node *RedBlackNode) error {
	return nil
}
