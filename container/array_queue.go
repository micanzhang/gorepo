package container

import "sync"

type ArrayQueue struct {
	sync.Mutex
	elems []interface{}
	size  int
}

func NewArrayQueue() Queue {
	return &ArrayQueue{
		elems: make([]interface{}, 0),
	}
}

func (q *ArrayQueue) Size() int {
	q.Lock()
	defer q.Unlock()

	return q.size
}

func (q *ArrayQueue) IsEmpty() bool {
	return q.Size() == 0
}

func (q *ArrayQueue) Enqueue(e interface{}) {
	q.Lock()
	defer q.Unlock()

	q.elems = append(q.elems, e)
	q.size++
}

func (q *ArrayQueue) Dequeue() interface{} {
	if q.IsEmpty() {
		return nil
	}

	q.Lock()
	defer q.Unlock()

	e := q.elems[0]
	q.elems = q.elems[1:]
	q.size--

	return e
}
