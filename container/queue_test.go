package container

import "testing"

func testQueue(q Queue, t testing.TB) {
	if !q.IsEmpty() {
		t.Error("expected: true, got: false")
	}

	if s := q.Size(); s != 0 {
		t.Errorf("expected: 0, got: %d", s)
	}

	total := 100
	for i := 0; i < total; i++ {
		q.Enqueue(i)
	}

	if q.IsEmpty() {
		t.Error("expected: false, got: true")
	}

	if s := q.Size(); s != total {
		t.Errorf("expected: %d, got: %d", total, s)
	}

	for i := 0; i < total; i++ {
		v := q.Dequeue()
		if v.(int) != i {
			t.Errorf("expected: %d, got: %d", i, v)
		}
	}
}
