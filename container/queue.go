package container

type Queue interface {
	Enqueue(interface{})
	Dequeue() interface{}
	IsEmpty() bool
	Size() int
}
