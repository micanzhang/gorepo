package container

import "testing"

func testStack(s Stack, t testing.TB) {
	if empty := s.IsEmpty(); !empty {
		t.Error("expected: true, got: false")
	}

	for i := 0; i < 10; i++ {
		s.Push(i)
	}

	for i := 10; i > 0; i-- {
		e1 := s.Peek()
		if e1.(int) != i-1 {
			t.Errorf("expected: %d, got: %d", i-1, e1.(int))
		}
		e2 := s.Pop()
		if e2.(int) != i-1 {
			t.Errorf("expected: %d, got: %d", i-1, e2.(int))
		}
	}

}

func TestArrayStack(t *testing.T) {
	s := NewArrayStack()

	testStack(s, t)
}

func TestLinkedListStack(t *testing.T) {
	s := NewLinkedListStack()

	testStack(s, t)
}
