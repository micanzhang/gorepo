package playground

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

func ipToInt(ip string) (int, error) {
	strs := strings.Split(ip, ".")
	if len(strs) != 4 {
		return 0, fmt.Errorf("invalid ip addr: %s", ip)
	}

	var n int
	for i, str := range strs {
		v, err := strconv.ParseInt(strings.Trim(str, " "), 10, 64)
		if err != nil {
			return 0, err
		}

		n = n | int(v)*int(math.Pow(2, float64((3-i)*8)))
	}

	return n, nil
}
