package playground

import "testing"

func TestIPToInt(t *testing.T) {
	ip := "172.168.5.1"
	v, err := ipToInt(ip)
	if err != nil {
		t.Fatal(err)
	}

	if v != 2896692481 {
		t.Errorf("expected:%d, got: %d", 2896692481, v)
	}

	ip1 := "172. 168.5 .1"
	v1, err := ipToInt(ip1)
	if err != nil {
		t.Fatal(err)
	}

	if v1 != 2896692481 {
		t.Errorf("expected:%d, got: %d", 2896692481, v1)
	}

	ip2 := "172.16 8.5.1"
	if _, err := ipToInt(ip2); err == nil {
		t.Error("expected: invalid ip addr, got: nil")
	}
}
