package dp

func maxSubArray(nums []int) int {
	var (
		n   = len(nums)
		dp  = make([]int, n)
		max int
	)

	if n == 0 {
		return max
	}

	for i, num := range nums {
		if i == 0 {
			dp[i] = num
			max = num
			continue
		} else if dp[i-1] > 0 {
			dp[i] = dp[i-1] + num
		} else {
			dp[i] = num
		}

		if max < dp[i] {
			max = dp[i]
		}

	}

	return max
}
