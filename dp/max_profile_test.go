package dp

import "testing"

func TestMaxProfile(t *testing.T) {
	type TC struct {
		nums []int
		mp   int
	}

	var tcs = []TC{
		TC{[]int{7, 1, 5, 3, 6, 4}, 5},
		TC{[]int{7, 6, 4, 3, 1}, 0},
	}

	for i, tc := range tcs {
		if got := maxProfile(tc.nums); got != tc.mp {
			t.Errorf("[%d] expected: %d, got: %d", i, tc.mp, got)
		}
	}
}
