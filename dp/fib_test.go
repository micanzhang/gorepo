package dp

import "testing"

func testFib(t testing.TB, fib func(int) int) {
	cases := []int{0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144}

	for k, v := range cases {
		if k == 0 {
			continue
		}

		if got := fib(k); got != v {
			t.Errorf("[%d] expected: %d, got: %d", k, k, got)
		}
	}
}

func TestRecusiveFib(t *testing.T) {
	testFib(t, recusiveFib)
}

func TestMemoRecusiveFib(t *testing.T) {
	testFib(t, memoRecusiveFib)
}

func TestBottomUpDPFib(t *testing.T) {
	testFib(t, bottomUpDPFib)
}

func TestSpaceOptimisticDPFib(t *testing.T) {
	testFib(t, spaceOptimisticDPFib)
}
