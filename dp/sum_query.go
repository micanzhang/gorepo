package dp

type NumArray struct {
	data []int
}

func Constructor(nums []int) NumArray {
	return NumArray{data: nums}
}

func (this *NumArray) SumRange(i int, j int) int {
	var sum int
	if l := len(this.data); i < 0 || i >= l || j < 0 || j >= l {
		return sum
	}

	for k := i; k <= j; k++ {
		sum += this.data[k]
	}

	return sum
}
