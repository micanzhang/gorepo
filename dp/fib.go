package dp

func recusiveFib(n int) int {
	if n <= 2 {
		return 1
	}

	return recusiveFib(n-1) + recusiveFib(n-2)
}

func memoRecusiveFib(n int) int {
	var fib func(int, map[int]int) int
	fib = func(k int, memo map[int]int) int {
		if v, ok := memo[k]; ok {
			return v
		} else if k <= 2 {
			return 1
		}

		return fib(k-1, memo) + fib(k-2, memo)
	}

	return fib(n, make(map[int]int, n))
}

func bottomUpDPFib(n int) int {
	memo := make(map[int]int, n)
	for i := 1; i <= n; i++ {
		if i <= 2 {
			memo[i] = 1
			continue
		}

		memo[i] = memo[i-1] + memo[i-2]
	}

	return memo[n]
}

func spaceOptimisticDPFib(n int) int {
	var a, b int // a: n-1 b: n-2
	for i := 1; i <= n; i++ {
		if i <= 2 {
			b, a = a, 1
			continue
		}

		b, a = a, a+b
	}

	return a
}
