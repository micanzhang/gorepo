package dp

import "strings"

func textJustification(text string, width int) []string {
	// split text to words
	words := strings.Split(text, " ")
	var res = make([]string, 0)
	//
	var line string
	for _, word := range words {
		if len(line+" "+word) > width {
			res = append(res, line)
			line = ""
			continue
		}

		line += " " + word
	}

	return res
}
