package dp

import "testing"

func TestTextJustification(t *testing.T) {
	text := "After a successful login, the user should be informed about the last successful or unsuccessful access date/time so that he can detect and report suspicious activity. Further information regarding logging can be found in the Error Handling and Logging section of the document. Moreover, it is also recommended to use a constant time comparison function while checking passwords in order to prevent timing attack. The latter consists of analyzing the difference of time between multiple requests with different inputs. In this case, a standard comparison of the form record == password would return false at the first character that does not match. The closer the submitted password, the longer the response time. By exploiting that, an attacker could guess the password"

	width := 80
	res := textJustification(text, width)
	for i, line := range res {
		if len(line) > width {
			t.Errorf("[%d] line(%s) greater than 80", i, line)
		}
	}

	t.Log(res)
}
