package dp

func maxProfile(nums []int) int {
	var (
		minp int // min price
		maxp int // max profile
	)

	for i, num := range nums {
		if i == 0 {
			minp = num
			continue
		}

		if num < minp {
			minp = num
		} else if maxp < num-minp {
			maxp = num - minp
		}
	}

	return maxp
}
