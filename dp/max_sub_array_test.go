package dp

import "testing"

func TestMaxSubArray(t *testing.T) {
	if got := maxSubArray([]int{-2, 1, -3, 4, -1, 2, 1, -5, 4}); got != 6 {
		t.Errorf("expected: %d, got:%d", 6, got)
	}
}
