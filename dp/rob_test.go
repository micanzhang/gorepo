package dp

import "testing"

func TestRob(t *testing.T) {
	if got := rob([]int{1, 6, 2, 7, 8, 6, 8, 8}); got != 27 {
		t.Errorf("expected:%d, got: %d", 16, got)
	}

	if got := rob([]int{2, 7, 9, 3, 1}); got != 12 {
		t.Errorf("expected:%d, got: %d", 12, got)
	}
}
