package dp

import "testing"

func TestSumQuery(t *testing.T) {
	na := Constructor([]int{-2, 0, 3, -5, 2, -1})

	tcs := [][]int{
		[]int{0, 2, 1},
		[]int{2, 5, -1},
		[]int{0, 5, -3},
	}

	for _, tc := range tcs {
		if got := na.SumRange(tc[0], tc[1]); got != tc[2] {
			t.Errorf("na.SumRange(%d, %d) expected: %d, got:%d", tc[0], tc[1], tc[2], got)
		}
	}
}
