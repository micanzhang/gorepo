package gorepo

import (
	"testing"

	"github.com/micanzhang/gorepo/container"
)

func TestMinStack(t *testing.T) {
	stack := container.NewArrayStack()
	ms := MinStack{stack, 0}

	ms.Push(6)
	ms.Push(7)
	ms.Push(2)

	if m := ms.GetMin(); m != 2 {
		t.Errorf("expected: %d, got: %d", 2, m)
	}

	if x := ms.Top(); x != 2 {
		t.Errorf("expected: %d, got: %d", 2, x)
	}

	ms.Pop()
	if m := ms.GetMin(); m != 6 {
		t.Errorf("expected: %d, got: %d", 6, m)
	}
}
