package gorepo

import (
	"testing"

	"github.com/micanzhang/acode/container"
)

func TestMaxStack(t *testing.T) {
	s := container.NewArrayStack()
	ms := MaxStack{s, 0}

	tcs := [][]int{
		[]int{5, 5},
		[]int{15, 15},
		[]int{2, 15},
		[]int{12, 15},
		[]int{22, 22},
		[]int{25, 25},
		[]int{1, 25},
	}

	for _, tc := range tcs {
		ms.Push(tc[0])
	}

	for i := len(tcs) - 1; i >= 0; i-- {
		tc := tcs[i]

		if x := ms.Top(); x != tc[0] {
			t.Errorf("[%d] expected: %d, got: %d", i, tc[0], x)
		}

		if m := ms.GetMax(); m != tc[1] {
			t.Errorf("[%d] expected: %d, got: %d", i, tc[1], m)
		}

		ms.Pop()
	}
}
